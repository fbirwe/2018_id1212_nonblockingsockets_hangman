/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.client.net;

import java.net.InetSocketAddress;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Message;

/**
 *
 * @author fredericbirwe
 */
public interface CommunicationListener {

    public void recvdMsg(Message msg);

    public void connected(InetSocketAddress serverAddress);

    public void disconnected();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.client.view;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.nio_sockets.hangman.client.net.ClientNet;
import se.kth.id1212.fbirwe.nio_sockets.hangman.client.net.CommunicationListener;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Constants;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.DisconnectMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GameEndMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GuessCharMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GuessWordMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Message;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.MsgType;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.NewGameMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.ResetMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.StatusMessage;

/**
 *
 * @author fredericbirwe
 */
public class HangmanView implements Runnable {
    private static final String PROMPT = "> ";
    private ClientNet serverConnection;
    Scanner sc;
    volatile InputState inputState;
    

    public HangmanView() {
        this.sc = new Scanner(System.in);
        inputState = InputState.GUESS;
        serverConnection = new ClientNet();
        serverConnection.connect(Constants.IP, Constants.PORT);
        
        serverConnection.addCommunicationListener(new ConsoleOutput());
        
        this.start();
    }
    
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                String next = getInput();

                if(inputState == InputState.RETRY) {
                    boolean retry = getYesOrNo(next);
                    retry(retry);
                } else if(inputState == InputState.GUESS) {
                    if(next.trim().toLowerCase().equals("rules")) {
                        showRules();
                    } else {
                        if(next.length() == 1) {
                            guess(next.charAt(0));
                        } else {
                            guess(next);
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(HangmanView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void guess(String s) throws IOException {
        serverConnection.sendMessage( new GuessWordMessage(s) );
    }
    
    public void guess(char c) throws IOException {
        serverConnection.sendMessage( new GuessCharMessage(c) );
    }
    
    public void retry(boolean newGame) throws IOException {
        setInputState(InputState.DISABLED);
        
        if ( newGame ) {
            serverConnection.sendMessage( new ResetMessage() );
        } else {
            serverConnection.sendMessage( new DisconnectMessage() );
        }
    }
    
    public void showRules() {
        safePrintln( Texts.RULES );
        safePrint(PROMPT);
    }
    
    private String getInput() {
        return sc.nextLine();
    }
    
    /**
     * The method receives an input from the user and brings it down to yes or no.
     * Whitespace in the beginning or the end of the input, lower- and uppercase and interpunction are ignored.
     * Valid inputs for yes are "yes" and "y", everything else is interpreted as "no"
     * @return yes or no
     */
    private boolean getYesOrNo() {
        String input = getInput().trim().toLowerCase().replaceAll("\\W", "");
        
        return (input.equals("yes") || input.equals("y"));
    }
    
    private boolean getYesOrNo(String in) {
        String input = in.trim().toLowerCase().replaceAll("\\W", "");
        
        return (input.equals("yes") || input.equals("y"));
    }
    
    private synchronized void setInputState( InputState inputState ) {
        this.inputState = inputState;
    }
    
    private synchronized void safePrint(String s) {
        System.out.print(s);
    }
    
    private synchronized void safePrintln(String s) {
        System.out.println(s);
    }
        
    private class ConsoleOutput implements CommunicationListener {
        @Override
        public void recvdMsg(Message msg) {
            
            if ( null != msg.getType() ) switch (msg.getType()) {
                case STATUS:
                    printToConsole(Texts.showStatus((StatusMessage) msg));
                    break;
                case GAME_WON:
                case GAME_LOST:
                    setInputState(InputState.RETRY);
                    printToConsole(msg.getType() == MsgType.GAME_WON ? Texts.GAME_WON : Texts.GAME_LOST, false);
                    printToConsole( Texts.showSolution( (GameEndMessage) msg ) , false);
                    printToConsole(Texts.showScore((GameEndMessage) msg), false);
                    printToConsole( Texts.TRY_AGAIN );
                    break;
                case NEW_GAME:
                    printToConsole( Texts.showWelcome( (NewGameMessage) msg ) );
                    setInputState(InputState.GUESS);
                    break;
                default:
                    break;
            }  
        }

        @Override
        public void connected(InetSocketAddress serverAddress) {
            printToConsole("Connected to " + serverAddress.getHostName() + ":"
                           + serverAddress.getPort());
        }

        @Override
        public void disconnected() {
            printToConsole("Disconnected from server.");
            System.exit(0);
        }
        
        private void printToConsole(String output) {
            printToConsole(output, false);
        }

        private void printToConsole(String output, boolean withPrompt) {
            safePrintln(output);
            if(withPrompt) {
                safePrint(PROMPT);
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.client.view;

import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GameEndMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.NewGameMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.StatusMessage;

/**
 *
 * @author fredericbirwe
 */
public class Texts {
    public final static String PROMPT = "> ";
    public final static String GAME_WON = "You won the game!";
    public final static String GAME_LOST = "You lost the game.";
    public final static String TRY_AGAIN = "Do you want to try again? (Type y or yes for yes)";
    public final static String RULES = "Type in a single character to guess if it is in the word we are looking for.\n"
            + "Upper- and lowercase are ignored.\n"
            + "Type in a whole word if you think you know the word we are looking for.\n"
            + "You got a number of attempts equal to the number of letters in the word.\n"
            + "If you are running out of attempts, you lost the game.\n"
            + "Type in \"rules\" during the game to see the rules again.";
    
    public static String showStatus( StatusMessage msg ) {
        StringBuilder out = new StringBuilder();
                                
        for(int i = 0; i < msg.getWord().length(); i++) {
            char curChar = msg.getWord().charAt(i);
            if( msg.getCorrectGuessed().contains( Character.toLowerCase(curChar) ) ) {
                out.append(curChar);
            } else {
                out.append('_');
            }
        }

        return (out.toString() + " – " + msg.getRemainingAttempts() + " attempts remaining.");

    }
    
    public static String showScore(GameEndMessage msg) {
        return "Your score is " + msg.getScore();
    }

    public static String showSolution(GameEndMessage msg) {
        return "The word we were looking for was: " + msg.getWord();
    }
    
    public static String showWelcome( NewGameMessage msg ) {
        return "Welcome! Have fun with the game.\n"
                + "Your word has " + msg.getWordLength() + " letters.\n"
                + "You have " + msg.getRemainingAttempts() + " attempts.";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class Constants {
    public static final int PORT = 8083; 
    public static final String IP = "localhost";
    public static final String DICTIONARY = "words";
    public final static String ATTRIBUTE_DELIMETER = "##";
    public final static String HEADER_DELIMETER = "###";
    public final static int MAX_MSG_LENGTH = 8192;
}

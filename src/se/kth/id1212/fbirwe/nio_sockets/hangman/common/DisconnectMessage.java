/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class DisconnectMessage extends Message implements MessageString{
    
    public DisconnectMessage() {
        super(MsgType.DISCONNECT);        
    }
    
}

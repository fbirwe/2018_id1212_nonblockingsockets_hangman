/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

import java.util.StringJoiner;

/**
 *
 * @author fredericbirwe
 */
public class GameEndMessage extends Message {
    private int score;
    private String word;
    
    public GameEndMessage(boolean isWon, int score, String word) {
        super(isWon ? MsgType.GAME_WON : MsgType.GAME_LOST);
        this.score = score;
        this.word = word;        
    }
    
    /**
     *
     * @param attributes
     */
    public GameEndMessage( String[] attributes ) {
        this( attributes[0].equals("GAME_WON"), Integer.parseInt(attributes[1]), attributes[2] );
    }
    
    public int getScore() {
        return this.score;
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public String toMessageString() {
        StringJoiner joiner = new StringJoiner( Constants.ATTRIBUTE_DELIMETER);
        joiner.add(this.score + "");
        joiner.add(this.word);
        
        return super.toMessageString() + joiner.toString();
    }
    
}
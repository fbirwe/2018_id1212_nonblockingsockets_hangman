/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class GuessCharMessage extends Message {
    private char c;
    
    public GuessCharMessage(char c) {
        super(MsgType.GUESS_CHAR);
        this.c = c;        
    }
    
    public GuessCharMessage( String[] attributes ) {
        this( attributes[1].charAt(0) );
    }
    
    public char getChar() {
        return this.c;
    }
    
    @Override
    public String toMessageString() {
        return super.toMessageString() + this.c + Constants.ATTRIBUTE_DELIMETER;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class GuessWordMessage extends Message {
    private String word;
   
    public GuessWordMessage(String word) {
        super(MsgType.GUESS_WORD);
        this.word = word;        
    }
    
    public GuessWordMessage( String[] attributes ) {
        this( attributes[1] );
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public String toMessageString() {
        return super.toMessageString() + this.word + Constants.ATTRIBUTE_DELIMETER;
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class Helper {   
    public static String messageToString( Message msg ) {
        String messageString = msg.toMessageString();
        int length = messageString.length();
                
        return length + Constants.HEADER_DELIMETER + messageString;
    }
    
    public static Message stringToMessage( String input ) throws WrongMessageSizeException {
        String[] splittedMessage = input.split(Constants.HEADER_DELIMETER);
        int sendedLength = Integer.parseInt(splittedMessage[0]);
                
        String[] attributes = splittedMessage[1].split(Constants.ATTRIBUTE_DELIMETER);


        if( sendedLength == splittedMessage[1].length() ) {
            Message msg = null;
        
            if( attributes[0].equals( MsgType.DISCONNECT.toString() )) {
                msg = new DisconnectMessage();
            } else if ( attributes[0].equals( MsgType.GAME_LOST.toString() )) {
                msg = new GameEndMessage( attributes );
            } else if ( attributes[0].equals( MsgType.GAME_WON.toString() )) {
                msg = new GameEndMessage( attributes );
            } else if ( attributes[0].equals( MsgType.GUESS_CHAR.toString() )) {
                msg = new GuessCharMessage( attributes );
            } else if ( attributes[0].equals( MsgType.GUESS_WORD.toString() )) {
                msg = new GuessWordMessage( attributes );
            } else if ( attributes[0].equals( MsgType.NEW_GAME.toString() )) {
                msg = new NewGameMessage ( attributes );
            } else if ( attributes[0].equals( MsgType.RESET.toString() )) {
                msg = new ResetMessage();
            } else if ( attributes[0].equals( MsgType.STATUS.toString() )) {
                msg = new StatusMessage( attributes );
            }

            return msg;
            
        } else {
            throw new WrongMessageSizeException();
        }
        
       
    }
}

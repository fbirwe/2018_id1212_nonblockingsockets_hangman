/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class Message implements MessageString{
    private MsgType type;
    
    public Message(MsgType type) {
        this.type = type;
    }
    
    public MsgType getType() {
        return this.type;
    }

    public String toMessageString() {
        return this.type.toString() + Constants.ATTRIBUTE_DELIMETER;
    }
    
}

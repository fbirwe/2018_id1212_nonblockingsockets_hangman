/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public enum MsgType {
    DISCONNECT, GAME_LOST, GAME_WON, GUESS_CHAR, GUESS_WORD, NEW_GAME, RESET, STATUS
}

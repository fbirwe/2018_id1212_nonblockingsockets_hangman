/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

import java.util.StringJoiner;

/**
 *
 * @author fredericbirwe
 */
public class NewGameMessage extends Message {
    private int wordLength;
    private int remainingAttempts;
   
    public NewGameMessage(int wordLength, int remainingAttempts) {
        super(MsgType.NEW_GAME);
        this.wordLength = wordLength;   
        this.remainingAttempts = remainingAttempts;
    }
    
    public NewGameMessage( String[] attributes ) {
        this( Integer.parseInt(attributes[1]), Integer.parseInt(attributes[2]) );
    }
    
    public int getWordLength() {
        return this.wordLength;
    }
    
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }
    
    @Override
    public String toMessageString() {
        StringJoiner joiner = new StringJoiner( Constants.ATTRIBUTE_DELIMETER );
        joiner.add(this.wordLength + "");
        joiner.add(this.remainingAttempts + "");       
        
        return super.toMessageString() + joiner.toString();
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

import java.util.ArrayList;
import java.util.StringJoiner;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.model.HangmanGame;

/**
 *
 * @author fredericbirwe
 */
public class StatusMessage extends Message{
    
    private ArrayList<Character> correctGuessed;
    private String word;
    private int remainingAttempts;
    
    public StatusMessage(HangmanGame curGame) {
        super(MsgType.STATUS);
        
        this.correctGuessed = curGame.getCorrectGuessed();
        this.word = curGame.getWord();
        this.remainingAttempts = curGame.getRemainingAttempts();        
    }
    
    public StatusMessage( String[] attributes ) {
        super(MsgType.STATUS);
        
        ArrayList<Character> correctGuessed = new ArrayList<>();
        for(int i = 0; i < attributes[1].length(); i++) {
            correctGuessed.add( attributes[1].charAt(i) );
        }
        
        this.correctGuessed = correctGuessed;
        this.word = attributes[2];
        this.remainingAttempts = Integer.parseInt(attributes[3]);
    }
    
    public ArrayList<Character> getCorrectGuessed() {
        return this.correctGuessed;
    }
    
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }
    
    public String getWord() {
        return this.word;
    }
    
    @Override
    public String toMessageString() {
        StringJoiner joiner = new StringJoiner( Constants.ATTRIBUTE_DELIMETER);
        joiner.add(this.correctGuessed.toString());
        joiner.add(this.word);
        joiner.add(this.remainingAttempts + "");
        
        return super.toMessageString() + joiner.toString();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class WrongMessageSizeException extends Exception {

    public WrongMessageSizeException() {
        super("The received message does not have the expected size");
    }
    
}

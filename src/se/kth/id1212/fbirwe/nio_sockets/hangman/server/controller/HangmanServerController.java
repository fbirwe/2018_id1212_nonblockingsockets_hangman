/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.controller;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CompletableFuture;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.model.HangmanGame;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.model.HangmanPlayer;

/**
 *
 * @author fredericbirwe
 */
public class HangmanServerController extends Observable {
    private HangmanPlayer player;
    
    public HangmanServerController() {
        this.player = new HangmanPlayer();        
    }
    
    public HangmanGame getCurGame() {
        return player.getCurGame();
    }
    
    public int getScore() {
        return player.getScore();
    }
    
    public String getWord() {
        return player.getWord();
    }
    
    public void guess(char c) {
        player.guess(c);
    }
    
    public void guess(String s) {
        player.guess(s);
    }

    public boolean  hasLost() {
        return player.hasLost();
    }
    
    public boolean hasWon() {
        return player.hasWon();
    }
    
    public int getRemainingAttempts() {
        return player.getCurGame().getRemainingAttempts();
    }
  
    public void restart() {
        start();
    }
    
    /**
     * This method is used to perform the creation of a new HangmanGame Instance (which includes a fileload in the constructor) asynchronously.
     * The observer pattern is used to notify the ClientHandler when a new Game is created. The ClientHandler then sends a new message to the client.
     */
    public void start() {
        CompletableFuture.runAsync(
            () -> {
                player.start();
                setChanged();
                notifyObservers();
            }
        );
    }

}

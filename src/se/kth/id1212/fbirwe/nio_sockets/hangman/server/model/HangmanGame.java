/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static se.kth.id1212.fbirwe.nio_sockets.hangman.common.Constants.DICTIONARY;

/**
 *
 * @author fredericbirwe
 */
public class HangmanGame {
    private String word;
    private int remainingAttempts;
    private ArrayList<Character> alreadyGuessed = new ArrayList<>();
    private ArrayList<Character> correctGuessed = new ArrayList<>();

    public HangmanGame() {
        try {
            URL url = getClass().getResource( DICTIONARY + ".txt");
            BufferedReader dictionary = new BufferedReader(new FileReader( url.getPath() ) );
            ArrayList<String> allWords = new ArrayList<>();
            
            String line;
            while ((line = dictionary.readLine()) != null) {
               if(line.length() > 1) {
                   allWords.add(line);
               }
            }
            
            word = allWords.get((int)(Math.random() * allWords.size()));
            remainingAttempts = word.length();
            
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(HangmanGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void guess(char c) {
        if(Character.isLetter(c) && !alreadyGuessed.contains(c) && remainingAttempts > 0 && !isSolved()) {
            c = Character.toLowerCase(c);
            boolean isInWord = false;
            for(int i = 0; i < word.length(); i++) {
                if(c == Character.toLowerCase(word.charAt(i))) {
                    isInWord = true;
                    correctGuessed.add(c);
                    break;
                }
            }
            
            if(!isInWord) {
                remainingAttempts--;
            }
            
            alreadyGuessed.add(c);
        }
    }
    
    public void guess(String s) {
        if(remainingAttempts > 0 && s.equals(word)) {
            for(int i = 0; i < word.length(); i++) {
                if(!correctGuessed.contains(Character.toLowerCase(word.charAt(i)))) {
                    alreadyGuessed.add(Character.toLowerCase(word.charAt(i)));
                    correctGuessed.add(Character.toLowerCase(word.charAt(i)));
                }
            }
        } else {
            remainingAttempts--;
        }
    }
    
    public boolean isLost() {
        return this.remainingAttempts <= 0 && !isSolved();
    }
    
    public boolean isSolved() {
        for(int i = 0; i < word.length(); i++) {
            char curChar = Character.toLowerCase( word.charAt(i) );
            
            if(!alreadyGuessed.contains( curChar )){
                return false;
            }
        }
        
        return true;
    }
    
    public boolean isWon() {
        return this.remainingAttempts >= 0 && isSolved();
    }
    
    public String toString() {
        StringBuilder out = new StringBuilder();
        
        for(int i = 0; i < word.length(); i++) {
            if(alreadyGuessed.contains(word.charAt(i))) {
                out.append(word.charAt(i));
            } else {
                out.append('_');
            }
        }
        
        return out.toString();
    }
    
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }
    
    public ArrayList<Character> getCorrectGuessed() {
        return correctGuessed;
    }
    
    public String getWord() {
        return this.word;
    }
    
}

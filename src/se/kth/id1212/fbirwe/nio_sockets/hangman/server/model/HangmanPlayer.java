/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.model;

/**
 *
 * @author fredericbirwe
 */
public class HangmanPlayer {
    private int score = 0;
    private int won = 0;
    private int lost = 0;
    private HangmanGame curGame = null;
    
    public void start() {
        curGame = new HangmanGame();
    }
    
    private void setScore() {
        if(hasWon()) {
            score++;
            won++;
        }
        
        if(hasLost()) {
            score--;
            lost++;
        }
    }
    
    public void guess(char c) {
        this.curGame.guess(c);
        
        setScore();
    }
    
    public void guess(String s) {
        this.curGame.guess(s);
        
        setScore();
    }
    
    public boolean hasWon() {
        return this.curGame.isWon();
    }
    
    public boolean  hasLost() {
        return this.curGame.isLost();
    }
    
    public HangmanGame getCurGame() {
        return this.curGame;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public String getWord() {
        return this.curGame.getWord();
    }
}

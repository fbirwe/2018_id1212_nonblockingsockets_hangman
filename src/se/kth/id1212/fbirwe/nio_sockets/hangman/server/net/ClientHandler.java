/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.net;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Constants;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GameEndMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GuessCharMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.GuessWordMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Helper;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Message;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.MsgType;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.NewGameMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.StatusMessage;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.WrongMessageSizeException;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.controller.HangmanServerController;

/**
 *
 * @author fredericbirwe
 */
public class ClientHandler {
    private HangmanServer server;
    private SocketChannel playerChannel;
    private HangmanServerController controller;
    private ControllerObserver controllerObserver;
    private final ByteBuffer msgFromClient = ByteBuffer.allocateDirect(Constants.MAX_MSG_LENGTH);
    private Queue<Message> remainingMessages;

    
    public ClientHandler(HangmanServer server, SocketChannel playerChannel) {
        this.server = server;
        this.playerChannel = playerChannel;
        this.controllerObserver = new ControllerObserver();
        this.controller = new HangmanServerController();
        this.remainingMessages = new ArrayDeque<>();      
        
        this.controller.addObserver(controllerObserver);
        this.controller.start();
    }

    public void run() {
        while( !remainingMessages.isEmpty() ) {
            Message msg = remainingMessages.poll();
              
              switch (msg.getType()) {
                case DISCONNECT : 
                    {
                        try {
                            disconnectClient();
                        } catch (IOException ex) {
                            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                case GUESS_CHAR :
                    controller.guess(((GuessCharMessage) msg).getChar());
                    break;
                case GUESS_WORD :
                    controller.guess(((GuessWordMessage) msg).getWord());
                    break;
                case RESET :
                    controller.restart();
                    break;
            }
              
            if(msg.getType() != MsgType.RESET) {
                Message out;

                if(controller.hasWon()) {
                    out = new GameEndMessage(true, controller.getScore(), controller.getWord());
                } else if (controller.hasLost()) {
                    out = new GameEndMessage(false, controller.getScore(), controller.getWord());
                } else {
                    out = new StatusMessage(controller.getCurGame());                        
                }
                                
                  try {
                      sendMessage(out);
                  } catch (IOException ex) {
                      Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
                  }

            }
        }        
    }
    
    void recvMsg() throws IOException {
        msgFromClient.clear();
        int numOfReadBytes;
        numOfReadBytes = playerChannel.read(msgFromClient);
        if (numOfReadBytes == -1) {
            throw new IOException("Client has closed connection.");
        }
        
        Message msg;
        try {
            msg = extractMessageFromBuffer();
            
            if(msg != null) {
                this.remainingMessages.add(msg);
            }

            //ForkJoinPool.commonPool().execute(this);
            run();
            
        } catch (WrongMessageSizeException ex) {
            System.out.println(ex.getMessage());
        }
                

    }
    
    void sendMessage(Message msg) throws IOException {
        playerChannel.write(ByteBuffer.wrap( (Helper.messageToString(msg)).getBytes() ));
    }
    
    private Message extractMessageFromBuffer() throws WrongMessageSizeException {
        msgFromClient.flip();
        byte[] bytes = new byte[msgFromClient.remaining()];
        msgFromClient.get(bytes);
        
        Message msg = Helper.stringToMessage( new String(bytes) );
        
        return msg;
    }
    
    void disconnectClient() throws IOException {
        playerChannel.close();
    }
    
    private class ControllerObserver implements Observer {
        @Override
        public void update(Observable o, Object arg) {
            try {
                HangmanServerController controller = (HangmanServerController) o;
                sendMessage(new NewGameMessage(controller.getWord().length(), controller.getRemainingAttempts()));

            } catch (IOException ex) {
            }
        }
    }
}

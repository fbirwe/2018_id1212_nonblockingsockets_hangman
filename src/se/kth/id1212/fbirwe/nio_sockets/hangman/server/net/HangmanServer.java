/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.StandardSocketOptions;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.nio_sockets.hangman.common.Constants;

/**
 *
 * @author fredericbirwe
 */
public class HangmanServer {
    private static final int LINGER_TIME = 5000;
    private Selector selector;
    private InetAddress hostIp; 
    
    public void serve() {
        try {
            selector = Selector.open();
            hostIp = InetAddress.getByName("localhost");

            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.bind( new InetSocketAddress(this.hostIp, Constants.PORT) );
            serverChannel.register( selector, SelectionKey.OP_ACCEPT );
            
            System.out.println("Server listening on " + this.hostIp.toString() + ":" + Constants.PORT);
            
        } catch (Exception e) {
            System.out.println("establishing server failed");
        }

        while( true ) {
            try {
                if( selector.select() <= 0 ) {
                    continue;
                } else {
                    processReadySet( selector.selectedKeys() );
                }            
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private void startHandler( SelectionKey key ) throws SocketException, IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();

        SocketChannel socketChannel = (SocketChannel) serverChannel.accept();
        socketChannel.configureBlocking(false);
                
        ClientHandler handler = new ClientHandler(this, socketChannel);
        socketChannel.register(selector, SelectionKey.OP_WRITE, handler);
        socketChannel.setOption(StandardSocketOptions.SO_LINGER, LINGER_TIME); //Close will probably                
    }
    
    private void processReadySet(Set<SelectionKey> selectedKeys) throws IOException {
        Iterator iterator = selectedKeys.iterator();
        
        while( iterator.hasNext() ) {
            SelectionKey key = (SelectionKey) iterator.next();
            iterator.remove();
            
            // Was für eine Art von Key ist das?
            if(key.isAcceptable()) {
                startHandler( key );                
            } else if( key.isReadable() ) {
                // read from client
                receiveFromClient(key);
                
            } else if( key.isWritable() ) {
                // write to client
                // this part is in fact no reachable
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }
    
    private void receiveFromClient( SelectionKey key ) {
        ClientHandler handler = (ClientHandler) key.attachment();
        try {
            handler.recvMsg();
        } catch (IOException clientHasClosedConnection) {
            try {
                removeClient(key);
            } catch (IOException ex) {
                Logger.getLogger(HangmanServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void removeClient(SelectionKey clientKey) throws IOException {
        ClientHandler handler = (ClientHandler) clientKey.attachment();
        handler.disconnectClient();
        clientKey.cancel();
    }
}

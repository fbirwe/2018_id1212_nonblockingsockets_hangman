/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.nio_sockets.hangman.server.startup;

import java.util.Scanner;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.model.HangmanGame;
import se.kth.id1212.fbirwe.nio_sockets.hangman.server.net.HangmanServer;

/**
 *
 * @author fredericbirwe
 */
public class HangmanServerStart {
    /**
     * @param args the command line arguments
     * This method only starts up the Hangman server
     */
    public static void main (String[] args) {
        HangmanServer server = new HangmanServer();
        server.serve();
    }
}
